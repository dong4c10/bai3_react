import React, { Component } from "react";

export default class Item_Shoes_Shop extends Component {
  render() {
    let { data, handleWatchDetail, handleBuyShoes,  } = this.props;
    let {img, name} = data;
    return (
      <div className="container">
        <div className="col-4">
          <div classname="card text-left">
            <img
              classname="card-img-top"
              src={this.props.data.image}
              style={{ width: "25%" }}
            />
            <div classname="card-body">
              <h4 classname="card-title">{this.props.data.name}</h4>
              <p classname="card-text">{this.props.data.alias}</p>
              <p classname="card-text">{this.props.data.price}</p>
            </div>
            <button
              onClick={() => {
                handleWatchDetail(data);
              }}
              className="btn btn-secondary"
            >
              Xem Chi Tiết
            </button>
            <button
              onClick={() => {
                handleBuyShoes(data);
              }}
              className="btn btn-secondary"
            >
              Mua
            </button>
          </div>
        </div>
      </div>
    );
  }
}
