import React, { Component } from "react";

export default class Detail_Shoes_Shop extends Component {
  render() {
    return (
      <div className="text-center">
        <table border={2}>
          <tr col="4">
            <th>Id</th>
            <th>Name</th>
            <th>Alias</th>
            <th>Price</th>
            <th>Description</th>
            <th>ShortDescription</th>
            <th>Quantity</th>
            <th>Image</th>
          </tr>
          <tr>
            <td>{this.props.detailShoe.id}</td>
            <td>{this.props.detailShoe.name}</td>
            <td>{this.props.detailShoe.alias}</td>
            <td>{this.props.detailShoe.price}</td>
            <td>{this.props.detailShoe.description}</td>
            <td>{this.props.detailShoe.shortDescription}</td>
            <td>{this.props.detailShoe.quantity}</td>
            <td >
              <img style={{width:"25%"}} src={this.props.detailShoe.image} alt="" />
            </td>
          </tr>
        </table>
      </div>
    );
  }
}
