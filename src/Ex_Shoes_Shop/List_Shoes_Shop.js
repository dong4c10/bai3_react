import React, { Component } from "react";
import Item_Shoes_Shop from "./Item_Shoes_Shop";

export default class List_Shoes_Shop extends Component {
  renderListShoes = () => {
    return this.props.list.map((item, index) => {
      return (
        <Item_Shoes_Shop
          key={index}
          data={item}
          handleWatchDetail={this.props.handleViewDetail}
          handleBuyShoes={this.props.handleBuy}

        />
      );
    });
  };
  render() {
    return <div className="row col-8">{this.renderListShoes()}</div>;
  }
}
