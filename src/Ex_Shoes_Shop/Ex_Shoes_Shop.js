import React, { Component } from "react";
import { shoesArr } from "./data";
import List_Shoes_Shop from "./List_Shoes_Shop";
import DetailShoe from "./Detail_Shoes_Shop";
import Card_Shoes from "./card_Shoes";

export default class Ex_Shoes_Shop extends Component {
  state = {
    shoesArr: shoesArr,
    detailShoe: shoesArr[0],
    card: [],
  };
  handleViewDetail = (shoes) => {
    this.setState({ detailShoe: shoes });
  };
  handleAddToCard = (shoes) => {
    let cloneCard = [...this.state.card];
    let index = cloneCard.findIndex((item) => item.id === shoes.id);
    if (index === -1) {
      let newShoe = { ...shoes, soLuong: 1 };
      cloneCard.push(newShoe);
    } else {
      cloneCard[index].soLuong = cloneCard[index].soLuong + 1;
    }
  
    // cloneCard.push(shoes);
    this.setState({ card: cloneCard });
    let newShoe = {
      ...shoes,
      soLuong: 1,
    };
    // cloneCard.push(newShoe);
    this.setState({ card: cloneCard });
  };
  handleRemoveFromCard = (shoes) => {
    let cloneCard = this.state.card.filter((item) => item.id !== shoes);
    this.setState({ card: cloneCard });
  };
  handleChangeAmount = (idShoe, option) =>{
    let cloneCard = [...this.state.card]
    let index = cloneCard.findIndex((item) => item.id === idShoe)
      cloneCard[index].soLuong = cloneCard[index].soLuong + option
      // soLuong có thể tăng hoặc giảm
      if(cloneCard[index].soLuong == 0){
        //sau khi updatee soLuong, nếu bằng 0 thì xóa
        cloneCard.splice(index, 1)
      }
  }
  render() {
    return (
      <div>
        <div className="row">
          <List_Shoes_Shop
            handleBuy={this.handleAddToCard}
            handleViewDetail={this.handleViewDetail}
            list={this.state.shoesArr}
          />
          <Card_Shoes
            handleChangeAmount={this.handleChangeAmount}
            handleChange={this}
            handleRemove={this.handleRemoveFromCard}
            card={this.state.card}
          />
          <div><p>Lúc anh bấm dấu cộng hay dấu trừ á, thì phải nhấn nút  mua lại thì nó mới hiện số lượng mới</p></div>
        </div>
        <DetailShoe detailShoe={this.state.detailShoe} />
      </div>
    );
  }
}
