import React, { Component } from "react";

export default class CardShoes extends Component {
  render() {
    console.log("test: ", this.props.card);
    let { card, handleRemove, handleChangeAmount } = this.props;
    return (
      <div className="col-6">
        <table class="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Amount</th>
              <th>Price</th>
              <th>Image</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {card.map((item, index) => {
              return (
                <tr key={index}>
                  <td>{item.name}</td>

                  <td>
                    <button
                      onClick={() => {
                        handleChangeAmount(item.id, +1);
                      }}
                      className="btn btn-success"
                    >
                      +
                    </button>
                    {item.soLuong}
                    <button
                      onClick={() => {
                        handleChangeAmount(item.id, -1);
                      }}
                      className="btn btn-warning"
                    >
                      -
                    </button>
                  </td>

                  <td>{item.price * item.soLuong}</td>

                  <td>
                    <img style={{ width: "50px" }} src={item.image} alt="" />
                  </td>
                  <td
                    onClick={() => {
                      handleRemove(item.id);
                    }}
                    className="btn btn-danger"
                  >
                    X{" "}
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
