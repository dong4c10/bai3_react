import logo from './logo.svg';
import './App.css';
import Ex_Shoes_Shop from './Ex_Shoes_Shop/Ex_Shoes_Shop'

function App() {
  return (
    <div className="App">
      <Ex_Shoes_Shop />
    </div>
  );
}

export default App;
